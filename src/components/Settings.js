import React from 'react';
import { AsyncStorage, StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { FontAwesome } from '@expo/vector-icons';
 
class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: '120'
        };
    };

    async componentDidMount() {
        try {
            let _time = await AsyncStorage.getItem('timeKey');
            if (_time === null) 
                this.setState({ time: '120' });
            else if (parseInt(_time) >= 3600)
                this.setState({ time: '3599' });
            else
                this.setState({ time: _time });
        } catch (error) {
            console.log("Retrieving token error: " + error);
        }
    }

    onMain = async () => {
        await AsyncStorage.setItem('timeKey', this.state.time);
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Main' })],
        });
        this.props.navigation.dispatch(resetAction);
    }

    onChange = (_time) => {
        this.setState({ time: _time });
    }
    
    render() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()} style={styles.container}>
                <Text style={styles.title}>Settings</Text>
                <View style={styles.inputContainer}>
                    <Text style={styles.secondsText}>Seconds:  </Text>
                    <TextInput
                        style={styles.secondsInput}
                        onChangeText={(_time) => this.onChange(_time)}
                        value={this.state.time}
                        keyboardType="number-pad"
                    />
                </View>
                
                <TouchableOpacity onPress={() => Keyboard.dismiss()} style={styles.check}>
                    <FontAwesome name="check-circle" size={60} color="white" />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.onMain} style={styles.home}>
                    <FontAwesome name="home" size={50} color="white" />
                </TouchableOpacity>
                <Text style={styles.copyright}>&copy; Matej Jurić</Text>
            </TouchableOpacity>
        );
    }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'steelblue',
        alignItems: 'center'
    },
    title: {
        fontSize: 45,
        marginTop: '12%',
        marginLeft: '-40%',
        color: 'white',
        marginBottom: 30,
        fontFamily: 'AppleSDGothicNeo-Regular'
    },
    inputContainer: {
        flexDirection: 'row',
        height: 60,
        alignItems: 'center'
    }, 
    secondsText: {
        color: 'white',
        fontSize: 25,
        fontFamily: 'AppleSDGothicNeo-Regular'
    },
    secondsInput: {
        height: 40,
        width: 85,
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor: 'white',
        fontSize: 30,
        fontFamily: 'AppleSDGothicNeo-Regular'
    },
    check: {
        position: 'absolute',
        paddingHorizontal: '0%',
        top: '35%'
    },
    home: {
        position: 'absolute',
        right: '5%',
        bottom: '5%'
    },
    copyright: {
        position: 'absolute',
        bottom: '1.5%',
        fontSize: 12,
        color: 'white'
    }
});
 
export default Main;