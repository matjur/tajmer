import React from 'react';
import { AsyncStorage, StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';
import { Feather, FontAwesome } from '@expo/vector-icons';
import CountDown from 'react-native-countdown-component';
 
class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: '120',
            running: false,
            timeFromStorage: '', 
            backgroundColor: 'steelblue' 
        };
    };

    async componentDidMount() {
        try {
            let _time = await AsyncStorage.getItem('timeKey');
            if (_time === null) 
                this.setState({ time: '120', timeFromStorage: '120' });
            else if (parseInt(_time) >= 3600)
                this.setState({ time: '3599' });
            else
                this.setState({ time: _time, timeFromStorage: _time });
        } catch (error) {
            console.log("Retrieving token error: " + error);
        }
    }

    onSettings = () => {
        this.props.navigation.navigate('Settings');
    }

    onReset = () => {
        this.setState({ 
            time: this.state.timeFromStorage, 
            running: false, 
            backgroundColor: 'steelblue'
        });
    }
    
    onChange = () => { 
        this.setState({ time: this.state.time - 1 })
    }

    onPause = () => {
        this.setState({ running: !this.state.running })
    }

    render() {
        return (
            <View style={[styles.container, {backgroundColor: `${this.state.backgroundColor}`}]}> 
                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Text style={{ fontSize: 35, color: 'white', marginLeft: '-38%', marginRight: '2%' }}>Tajmer</Text>
                    <Image source={require('./logo.png')} style={{ width: 35, height: 35, marginTop: 3 }} />
                </View>
                <View style={{ borderColor: 'white', borderWidth: 1, width: '85%' }}></View>
                <CountDown
                    until={parseInt(this.state.time)}
                    size={60}
                    onFinish={() => {this.setState({ backgroundColor: '#FF3333', running: false })}}
                    digitStyle={{backgroundColor: '#FFF'}}
                    digitTxtStyle={{color: `${this.state.backgroundColor}`, fontFamily: 'AppleSDGothicNeo-Regular'}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: 'min', s: 'sek'}}
                    onPress={this.onReset}
                    onChange={this.onChange}
                    running={this.state.running}
                    timeLabelStyle={styles.timeLabel}
                    style={{ marginTop: '10%' }}
                />
                {this.state.running ? 
                    <TouchableOpacity onPress={this.onPause}>
                        <FontAwesome name="pause" size={45} color="white" />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={this.onPause}>
                        <FontAwesome name="play" size={45} color="white" />
                    </TouchableOpacity>
                }
                <View style={{ borderColor: 'white', borderWidth: 1, width: '85%', marginTop: '6%' }}></View>
                <TouchableOpacity onPress={this.onSettings} style={styles.settingsButton}>
                    <Feather name="settings" size={40} color="white" />
                </TouchableOpacity>
            </View>
        )
    }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 35
    }, 
    title: {
        fontSize: 45,
        marginTop: '12%',
        color: 'white',
        marginLeft: '-50%',
        fontFamily: 'AppleSDGothicNeo-Regular'
    },
    settingsButton: {
        position: 'absolute',
        right: '5%',
        bottom: '5%' 
    }, 
    timeLabel: {
        color: 'white',
        fontFamily: 'AppleSDGothicNeo-Regular'
    }
});
 
export default Main;