import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import Main from './src/components/Main';
import Settings from './src/components/Settings';


const AppStackNavigator = createStackNavigator ({
  Main: {
    screen: Main,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  }, 
  Settings: {
    screen: Settings,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  }
});

const AppContainer = createAppContainer(AppStackNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
    );
  }
}